// #1 Разработайте функцию-конструктор, которая будет создавать объект Human (человек).
//  Создайте массив объектов и реализуйте функцию, которая будет сортировать элементы массива
//  по значению свойства Age по возрастанию или по убыванию.



    // создаем ф-ю конструктор(класс)
    function Human(name, age) {
        this.name = name ;
        this.age = age ;
    }

    // Выясняем колличество людей в массиве 
    let numHum = +prompt("Колличество людей");
    // создаем пустой массив на указанное кол-во людей
    const arrHuman = [numHum];

    document.write("Исходный массив: <br>");
    // выводим пользователю с помощью цикла for начальный массив объектов
    for (let i = 0; i < numHum; i++) {
        arrHuman[i] = new Human(prompt(`Введите имя ${i + 1} человека`), prompt(`Введите возраст ${i + 1} человека`));
        document.write(`${i + 1} объект: Имя ${arrHuman[i].name}, Возраст ${arrHuman[i].age} лет <br>`);
    };

    document.write("Итоговый массив: <br>");

    // функция для сортировки (по какому свойству объекта будет сортировать)
    Human.forSort = function(nameProp) {
        return (a, b) => a[nameProp] > b[nameProp] ? 1 : -1;
    }
    // сортировка массива объектов
    arrHuman.sort(Human.forSort("age"));

    // выводим пользователю с помощью цикла ForEach(для массивов) итоговый массив объектов сортированных по возростанию свойства "age"
    arrHuman.forEach((user, index) => document.write(`${index + 1} объект: Имя: ${user.name}, Возраст ${user.age} лет <br> `));


// #2 Разработайте функцию-конструктор, которая будет создавать объект Human (человек), добавляя на свое 
// усмотрение свойства и методы в этот объект.
// Подумайте, какие методы и свойства следует сделать уровня экземпляра, а какое уровня функции конструктора.

/*
    function Human(name, age, salary) {
        this.name = name; // свойство уровня экземпляра
        this.age = age; // свойство уровня экземпляра
        this.salary = salary; // свойство уровня экземпляра

        Human.CountUsers ++;
        if (Human.CountUsers > Human.MaxUsers) {
            console.error("Максимальное количество пользователей уже создано");
        }

        Human.totalSalary += this.salary; 

    }

    Human.prototype.show = function() {
        document.write(`My name is ${this.name}. I'm ${this.age} years old`)
    } // метод уровня экземпляра


    Human.averageSalary = function () {
        return Human.totalSalary / Human.CountUsers; // метод (подсчета средней заработной платы) функции конструктора
    }

    Human.MaxUsers = 3; // свойство функции конструктора
    Human.CountUsers = 0; // свойство функции конструктора
    Human.totalSalary = 0; // свойство функции конструктора

    h1 = new Human('Саша', '34', 500);

    h2 = new Human('Вася', '28', 800);

    h3 = new Human('Петя', '32', 500);

    // h4 = new Human('Коля', '22', 300);


    document.write(Human.averageSalary());
*/