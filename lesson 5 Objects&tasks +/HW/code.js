// Создать объект "Документ", в котором определить свойства "Заголовок, тело, футер, дата". 
// Создать в объекте вложенный объект - "Приложение". Создать в объекте "Приложение", 
// вложенные объекты, "Заголовок, тело, футер, дата". Создать методы для заполнения и 
// отображения документа.

let doc = {
    header: "",
    body: "",
    footer: "",
    date: "",
    app: {
        header: {},
        body: {},
        footer: {},
        date: {},
    },
    fillProp: function () {

        for (let key in this) {
            if (this[key] === "") {
                this[key] = prompt(`Введите свойство для ${key}`);
                document.getElementById("test").innerHTML += `<p>В данном объекте, ${key} имеет свойство: ${this[key]}</p>`;
            } else if (typeof (this[key]) === "object") {
                for (let keyApp in this[key]) {
                    this[key][keyApp] = prompt(`Введите значение для объекта app.${keyApp}`);
                    document.getElementById("test").innerHTML += `<p>Во вложенном объекте, ${keyApp} имеет свойство: ${ this[key][keyApp]}</p>`;
                }
            }
        }
    }

}
doc.fillProp();