// #1 Создайте объект заработных плат obj. Выведите на экран зарплату Пети и Коли.
// Этот объект дан:
// var obj = {'Коля':'1000', 'Вася':'500', 'Петя':'200'};

/*
    var obj = {
        'Коля': '1000',
        'Вася': '500',
        'Петя': '200'
    };

    function showSalary(userName) {

        let userArr = userName.split(' ');

        let result = userArr.map(item => {
                document.getElementById("test").innerHTML += `<div style="background-color: #d7d7d7; text-align: center">Зарплата ${item}: ${obj[item]} грн.</div>`;
            });

        return result;

    };

    showSalary(prompt("Укажите имена через ПОБЕЛ, чью зарплату надо узнать"));
*/

// #2 Создайте объект криптокошилек. В кошельке должно хранится имя владельца, несколько валют 
// Bitcoin, Ethereum, Stellar и в каждой валюте дополнительно есть имя валюты, логотип, несколько монет и курс
//  на сегодняшний день. Также в объекте кошелек есть метод при вызове которого он принимает имя валюты и выводит 
//  на страницу информацию. "Добрый день, ... ! На вашем балансе (Название валюты и логотип) осталось N монет, 
//  если вы сегодня продадите их то, получите ...грн.
//  Вывод на страницу должен быть касиво формлен с использованием css и html.

/*----------Чтобы работала стилизация, раскомменитруйте, пожалуйста, тэги в index.html-----------*/


    const wallet = {
        userName: "Mentor",
        bitcoin: {
            logo: "<img src='https://s2.coinmarketcap.com/static/img/coins/64x64/1.png' alt='bitcoin' /> ",
            abr: "btc",
            price: 21386.65,
            coins: 5
        },
        ethereum: {
            logo: " <img src='https://s2.coinmarketcap.com/static/img/coins/64x64/1027.png' alt='ethereum' /> ",
            abr: "eth",
            price: 2947.94,
            coins: 10
        },
        stellar: {
            logo: " <img src='https://s2.coinmarketcap.com/static/img/coins/64x64/512.png' alt='stellar' />",
            abr: "xlm",
            price: 0.1855,
            coins: 500
        },
        show: function (chooseCoin) {
            document.getElementById("header").innerHTML = `Добрий день, ${wallet.userName} !`;
            
            let coinPrice = wallet[chooseCoin].price;
            const exchangeRate = 30.2;
            const exchange = (b, a) => {
                return b * a * exchangeRate;
            };
            document.getElementById("coin-logo").innerHTML = `${wallet[chooseCoin].logo}`;
            document.getElementById("coin-name").innerHTML = `${chooseCoin}`;
            document.getElementById("coin-abr").innerHTML = `<p>${wallet[chooseCoin].abr}</p>`;
            document.getElementById("coin-num").innerHTML = `залишилось<p>${wallet[chooseCoin].coins}</p>монет`;
            document.getElementById("coin-bal").innerHTML = `Це: <p>${exchange(wallet[chooseCoin].coins, coinPrice).toFixed(2)}</p>`;
        
        }
    }
    wallet.show(prompt("Виберіть валюту для ознайомлення?", "bitcoin-ethereum-stellar"));


// #3 Напишите функцию isEmpty(obj), которая возвращает true, если у объекта нет свойств, иначе false.

/*
    var obj = {
        // 'Коля': '1000',
        // 'Вася': '500',
        // 'Петя': '200'
    };

    function isEmpty(object) {
        for (let key in object) {
                return false;
            } 
            return true;     
        }

        document.getElementById("test").innerHTML = isEmpty(obj);

*/


