var x = 6;
var y = 14;
var z = 4;

// #1

x += y - x++ * z;  // x = x + y - x++ * z; 6 + 14 - 6 * 4 = -4;

var x = 6;
var y = 14;
var z = 4;

//#2

z = --x - y * 5;  //  5 - 14 * 5 = -65;

var x = 6;
var y = 14;
var z = 4;

//#3

y /= x + 5 % z;   // y = y/(x + 5 % z); 14/(6 + 1) = 2;

var x = 6;
var y = 14;
var z = 4;

//#4

z - x++ + y * 5;   // 4 - 6 + 14 * 5 = 68;

var x = 6;
var y = 14;
var z = 4;

//#5

x = y - x++ * z;   // 14 - 6 * 4 = -10;

