
//  тікання банера скідки від миши кліента
const element = document.querySelector('#banner');

let maxElemMove = document.documentElement.clientWidth - element.offsetWidth;
let elemMove;

element.addEventListener('mousemove', move);

function move() {
    elemMove = Math.random() * maxElemMove;
    element.style.right = elemMove + 'px';
}

// ---------------------------------------------------------------------------------------

// валідація форми

let form = document.forms[1];
const button = document.querySelector('[name=btnSubmit]'), // знаходим кнопку submit по "name"
    resetBtn = document.querySelector('[name=cancel]'), // знаходим кнопку reset по "name"
    [...inputsArrAll] = document.querySelectorAll('.grid input'), // масив всіх input
    inputsArr = inputsArrAll.filter(item => item.hasAttribute('placeholder'));// масив інпутів тільки тих, яки будем перевіряти

let isValidArr = [], // допоміжний масив для перевірки валідності усіх інпутів
    result; // допоміжна змінна для перевірки якщо всі поля заповнені чи ні

// слухач подій по кожному input
form.addEventListener('keyup', validateInput);

// слухач подій щодо натискання кнопки submit
button.addEventListener('click', onButtonCheck);

// слухач подій щодо натискання кнопки reset
resetBtn.addEventListener('click', () => {
    inputsArr.forEach(el => {
        el.style.outline = 'inherit' 
    })
});

// функція слухача подій
function validateInput({
    target // об'єкт, за яким відбуватиметься подія
  }) {
    let pattern; // змінна для порівняння вступних даних за патерном (регулярним виразом)
  
    // інпут 1 (им'я, Укр. Букви)
    if (target.name === 'name') {
      pattern = /^[А-яЇїІіЄєҐґ]+$/;
      inputChek(pattern, target);
    }
    // інпут 2 (тел., у форматі +38ХХХХХХХХХ)
    if (target.name === 'phone') {
      pattern = /^\+38\d{3}\d{7}$/m;
      inputChek(pattern, target);
    }
    // інпут 3 (email)
    if (target.name === 'email') {
      pattern = /\b[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}\b/g;
      inputChek(pattern, target);
    }
  }

  // функція валідації input окремо, одночасно
function inputChek(pat, el) {
    if (pat.test(el.value)) {
      el.style.outline = '3px solid green'
     
      el.setAttribute('valid', '1');
    } else if (el.value === "") {
      el.style.outline = 'inherit'
     
      el.setAttribute('valid', '0');
    } else {
      el.style.outline = '3px solid red'
      el.nextElementSibling.classList.add('err');
   
      el.setAttribute('valid', '0');
    }
  }

  // функція при натисканні на кнопку "Підтвердити замовлення"
function onButtonCheck(e) {
    
    // заповнюємо допоміжний масив значеннями додаткового атрибуту valid
    inputsArr.forEach(element => {
        if (!element.hasAttribute('valid')) {
            element.setAttribute('valid', '0');
            isValidArr.push(element.getAttribute('valid'))
        } else {
            isValidArr.push(element.getAttribute('valid'))
        }
    })
  
    result = isValidArr.includes('0'); // перевіряємо на наявність значення "0" у допоміжному масиві (якщо є, то повернемо true)
  
    if (result === true) {
      console.error('Не всі поля заповнені');
      inputsArr.forEach(el => {
        // маркування, якщо всі елементи порожні або ті, які не вірно запалені
        if (el.getAttribute('valid') === '0' || !el.getAttribute('valid')) {  
          el.style.outline = '3px solid red';
        }
      })
      isValidArr = []; // обнулення допоміжного масиву, якщо користувач відразу виправлятиме
      e.preventDefault();
    } else {
      let valueArr = []; // допоміжний масив значень з input
      inputsArr.forEach(element => {
        valueArr.push(element.value)
      })
  
      // обнулення форми, після успішного заповнення та відправлення
      inputsArr.forEach(el => {
        el.style.outline = 'inherit'
        el.value = '';
        el.setAttribute('valid', '0');
      })
    
      e.preventDefault();
  
      console.log(valueArr);
    }
  }