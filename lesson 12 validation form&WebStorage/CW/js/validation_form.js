// Проверка данных.

//         Используя JS cоздайте 5 полей для воода данных и застилизуйте их. Добавьте стили на ошибку и 
//         стиль на верный ввод.
//         Поля:
//         Имя (Укр. Буквы), Номер телефона в формате +38ХХХ-ХХХ-ХХ-ХХ, електронная почта, пароль и 
//         подтверждение пароля (пароли должны совпадать).

//         Реазизуй проверку данных.
//         При вводе данных сразу проверять на правильность и выводить ошибку если такая необходима.
//         добавь кнопку регистрация, при нажатии кнопки проверить все поля и вывести ошибку если такая будет.

//         Сохраниение.
//         Если пользователь все записал верно, то сохраните данные на клиенте с указанием даты и времени сохранения.  

//         Для стилизации используй CSS классы. Для создания элементов используй JS.



let form = document.forms[0]; // переменная выбор формы
const button = document.querySelector('#button'); // находим кнопку submit
const [...inputsArr] = document.querySelectorAll('.check input'); // массив всех input, кроме кнопки submit
let isValidArr = []; // вспомогательный массив для проверки валидности всех инпутов
let result; // вспомогательная переменная для проверки если все поля заполнены или нет

// переменная с сообщениями об ошибках
const massage = {
  name: "Им'я повино бути кирилицею",
  phone: "Невірний номер",
  email: "Невірний email",
  passwordFirst: "Пароль від 3 до 10 символів і латиницею",
  passwordSecond: "Не співпадає пароль"
}

// слушатель событий по нажатию кнопки submit
button.addEventListener('click', onButtonCheck);

// слушатель событий по каждому input
form.addEventListener('keyup', validateInput);

// функция слушателя событий
function validateInput({
  target // объект, по которому будет происходить событие
}) {
  let pattern; // переменная для сравнивания вводных данных по паттерну (регулярному выражению)

  // инпут 1 (имя, Укр. Буквы)
  if (target.name === 'namemsg') {
    pattern = /^[А-яЇїІіЄєҐґ]+$/;
    inputChek(pattern, target, massage.name);
  }
  // инпут 2 (тел., в формате +38ХХХ-ХХХ-ХХ-ХХ)
  if (target.name === 'phonemsg') {
    pattern = /^\+38\d{3}-\d{3}-\d{2}-\d{2}$/m;
    inputChek(pattern, target, massage.phone);
  }
  // инпут 3 (email)
  if (target.name === 'email') {
    pattern = /\b[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}\b/g;
    inputChek(pattern, target, massage.email);
  }
  // инпут 4 (пароль, от 3 до 10 символов букв или цифр без учета регистра)
  if (target.name === 'password') {
    pattern = /^[A-Za-z0-9]{3,10}$/;
    inputChek(pattern, target, massage.passwordFirst);
  }
  // инпут 5 (пароль повторно, сравнение через фунцию compare, т.к. если сразу написать, то пересекаются условия выше)
  if (target.name === 'checkpassword') {
    compare(target);
  }
}

// функция валидации input по отдельности, сразу
function inputChek(pat, el, msg) {
  if (pat.test(el.value)) {
    el.style.outline = '1px solid green'
    el.nextElementSibling.classList.add('ok');
    el.nextElementSibling.classList.remove('err');
    el.previousElementSibling.innerText = '';
    el.setAttribute('valid', '1');
  } else if (el.value === "") {
    el.style.outline = 'inherit'
    el.nextElementSibling.classList.remove('err');
    el.nextElementSibling.classList.remove('ok');
    el.previousElementSibling.innerText = '';
    el.setAttribute('valid', '0');
  } else {
    el.style.outline = '1px solid red'
    el.nextElementSibling.classList.add('err');
    el.nextElementSibling.classList.remove('ok');
    el.previousElementSibling.innerText = msg;
    el.setAttribute('valid', '0');
  }
}

// функция сравнения passwordFirst и passwordSecond
function compare(element) {
  let a = document.forms[0][3].value; // значение в поле passwordFirst
  let b = document.forms[0][4].value; // значение в поле passwordSecond

  if (element.value === "") {
    element.style.outline = 'inherit'
    element.nextElementSibling.classList.remove('err');
    element.nextElementSibling.classList.remove('ok');
    element.previousElementSibling.innerText = '';
    element.setAttribute('valid', '0');
  } else if (a !== b) {
    element.style.outline = '1px solid red'
    element.nextElementSibling.classList.add('err');
    element.nextElementSibling.classList.remove('ok');
    element.previousElementSibling.innerText = massage.passwordSecond;
    element.setAttribute('valid', '0');
  } else {
    element.style.outline = '1px solid green'
    element.nextElementSibling.classList.add('ok');
    element.nextElementSibling.classList.remove('err');
    element.previousElementSibling.innerText = '';
    element.setAttribute('valid', '1');
  }
}

// функция при клике на кнопку "Регистрация"
function onButtonCheck(e) {
  
  // заполняем вспомогательный массив значениями дополнительного атрибута valid
  inputsArr.forEach(element => {
    isValidArr.push(element.getAttribute('valid'))
  })

  result = isValidArr.includes('0'); // проверяем на наличие значения "0" во вспомогательном массиве (если есть, то вернем true)


  if (result === true) {
    console.error('Не всі поля заповнені');
    inputsArr.forEach(el => {
      // маркиповка, если все элементы пусты или те, которые не верно заполены
      if (el.getAttribute('valid') === '0') {  
        el.style.outline = '1px solid red';
      }
    })
    isValidArr = []; // обнуление вспомогательного массива, если пользователь будет сразу исправлять
    e.preventDefault();
  } else {
    let valueArr = []; // вспомогательный массив значений из input
    inputsArr.forEach(element => {
      valueArr.push(element.value)
    })

    valueArr.push(new Date()); // добавляем дату и время
    valueArr.splice(4, 1); // вырезаем из массива значение проверочного пароля
    window.localStorage.user1 = valueArr; // запись массива в LocalStorage на клиенте


    // обнуление формы, после успешного заполнения и отправки
    inputsArr.forEach(el => {
      el.style.outline = 'inherit'
      el.nextElementSibling.classList.remove('err');
      el.nextElementSibling.classList.remove('ok');
      el.value = '';
      el.setAttribute('valid', '0');
    })

    // вывод информации клиенту об успешной регистрации
    const success = document.createElement('p');
    success.classList.add('success');
    success.innerText = 'Реєстрація пройшла успішно';
    button.after(success);


    e.preventDefault();

    console.log(valueArr);
  }
}