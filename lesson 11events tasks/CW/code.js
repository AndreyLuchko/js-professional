// #1 Создай класс, который будет создавать пользователей с именем и фамилией. Добавить к классу метод для вывода 
// имени и фамилии

/*
    class Users {
        constructor (name, surname) {
            this.name = name;
            this.surname = surname;
        }
        show () {
            console.log(`User name: ${this.name}; User surname: ${this.surname}`);
        }
    }
    const vasya = new Users("Vasya", "Pupkin");

    vasya.show();
*/

// #2 Создай список состоящий из 4 листов. Используя джс обратись к 2 li и с использованием навигации по DOM дай 1 
// элементу синий фон, а 3 красный

/*
    const first = document.querySelector('.task2 li:first-child'),
        third = document.querySelector('.task2 li:nth-child(3)');

    first.style.background = 'blue';
    third.style.background = 'red';
*/
/*-------------------------Другой способ----------------------------------------------------------------*/
/*
    const [...li] = document.querySelectorAll('li');
    li[0].style.background = 'blue';
    li[2].style.background = 'red';
*/

// #3 Создай див высотой 400 пикселей и добавь на него событие наведения мышки. При наведении мышки выведите текстом 
// координаты, где находится курсор мышки
/*
    const place = document.querySelector('.task3'),
        text = document.createElement('p');

    place.addEventListener('mouseover', (event) => {
        text.textContent = `Координата X: ${event.offsetX}; Координата Y: ${event.offsetY}`;
        place.append(text);
    });
*/

// #4 Создай кнопки 1,2,3,4 и при нажатии на кнопку выведи информацию о том какая кнопка была нажата

/*
    const buttons = document.querySelector('.task4'),
    p = document.querySelector('.task4 p');

    buttons.addEventListener('click', (event) => {
        let number = event.target.textContent;
        p.textContent = `Нажата конопка ${number}`;
    });
*/

// #5 Создай див и сделай так, чтобы при наведении на див, див изменял свое положение на странице

/*
    const move = document.querySelector('.task5');

    move.addEventListener('mouseover', () => {
       move.classList.add('move');
    });
    move.addEventListener('mouseout', () => {
        move.classList.remove('move');
     });
*/

// #6 Создай поле для ввода цвета, когда пользователь выберет какой-то цвет сделай его фоном body

/*
    const colorPicker = document.querySelector('.task6 input');

    colorPicker.addEventListener("change", watchColorPicker, false);

    function watchColorPicker(event) {
    const bg = document.querySelector("body")
        bg.style.background = event.target.value;
    }
*/

// #7 Создай инпут для ввода логина, когда пользователь начнет Вводить данные в инпут выводи их в консоль

/*
    const printConsole = document.querySelector('.task7');
    let result = '';
    printConsole.addEventListener('keypress', (event) => {
        result += event.key;
        console.log(`${result}`);
    });

*/

// #8 Создайте поле для ввода данных, поcле введения данных выведите текст под полем


const printText = document.querySelector('.task8'),
p = document.createElement('p');
printText.append(p);

let result = '';
printText.addEventListener('keypress', (event) => {
    result += event.key;
    p.textContent += result;
});

