
//  #1 Напиши функцию map(fn, array), которая принимает на вход функцию 
// и массив, и обрабатывает каждый элемент массива этой функцией, 
// возвращая новый массив.

/*
    const double = (x) => {
        return x * 2;
    }
    let arr = [1, 8, 14, 15, -45];


    function map(fn, array) {
        let newArray = [];
        for (let i = 0; i < array.length; i++) {
            const element = array[i];

            newArray.push(fn(element));
        }
        return newArray;
    }
    document.write(map(double, arr));
*/



// #2 Перепишите функцию, используя оператор '?' или '||'
// Следующая функция возвращает true, если параметр age больше 18. 
// В ином случае она задаёт вопрос confirm и возвращает его результат.
// function checkAge(age) {
//     if (age > 18) {
//         return true;
//     } else {
//         return confirm('Родители разрешили?');
//     }
// }


    function checkAge(age) {
        const a = age > 18 ? true : confirm('Родители разрешили?')
        return a;
    }
    document.write(checkAge(5));

