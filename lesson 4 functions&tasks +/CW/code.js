// #1 Сделайте функцию, которая принимает параметром число от 1 до 7, а возвращает день недели 
// на Украинском языке

/*

    let numberDay = prompt("Введіть число від 1 до 7", "1-2-3-4-5-6-7");

    function dayOfWeek (a) {

        switch (a) {
            case "1": document.getElementById("show").innerHTML = `<p>Число ${numberDay} це Понеділок</p>`;
                break;   
            case "2": document.getElementById("show").innerHTML = `<p>Число ${numberDay} це Вівторок</p>`;
                break;
            case "3": document.getElementById("show").innerHTML = `<p>Число ${numberDay} це Середа</p>`;
                break;
            case "4": document.getElementById("show").innerHTML = `<p>Число ${numberDay} це Четвер</p>`;
                break;
            case "5": document.getElementById("show").innerHTML = `<p>Число ${numberDay} це П'ятниця</p>`;
                break;
            case "6": document.getElementById("show").innerHTML = `<p>Число ${numberDay} це Субота</p>`;
                break;
            case "7": document.getElementById("show").innerHTML = `<p>Число ${numberDay} це Неділя</p>`;
                break;
            default:
            alert("Введіть число від 1 до 7");
        };
    }
    dayOfWeek(numberDay);

*/
/*------------------------------2-й вариант-----------------------------------------------------------*/

/*

    let dayNumber;

    function dayOfWeek() {
        let dayNumber = parseInt(prompt("Введіть число від 1 до 7", "1-7"));
        let dayArray = ["Понеділок", "Вівторок", "Середа", "Четверг", "П'ятниця", "Субота", "Неділя"];

        if (dayNumber < 1 || dayNumber > 7) {
            alert("Вкажить число від 1 до 7");
            return;
        }else {
            return dayArray[dayNumber - 1];   
        }  
    };
    document.write(dayOfWeek (dayNumber));

*/




// #2 Дана строка вида 'var_text_hello'. Сделайте из него текст 'VarTextHello'.

/*

    function transformText (text) {
        // Разбиваем строку на массив по заданному разделителю
        let textSplit = text.split('_');
        // С помощью метода "map" 0-й символ переводим в верхний регистр и создаем новый массив начиная с 1 символа
        let textMap = textSplit.map((item) => item[0].toUpperCase() + item.slice(1));
        //  объединяем все что получилось в строку
        let result = textMap.join('');
        return result;
    };
    document.getElementById("show").innerHTML = `<p style="color: red">${transformText('var_text_hello')}</p>`;

*/
/*------------------------------2-й вариант-----------------------------------------------------------*/

/*

    let output = "";

    function textTransorm(a) {

        for (let i = 0; i < a.length; i++) {
            let letterInput = a[i];
            if (i == 0) {
                output += a[0].toUpperCase();
            } else if (letterInput == "_") {
                output += a[i + 1].toUpperCase();
                i++;
            } else {
                output += letterInput;
            }
        }
        return output;
    }
    document.write(textTransorm("var_text_hello"));

*/

// #3 Создайте функцию которая будет заполнять массив 10-ю иксами с помощью цикла.

/*

    function fillArray(a, b) {
        let arr = [];
        for (let i = 0; i < b; i++) {
        arr.push(a);
        };
        return arr;
    };

    document.write(fillArray("x", 10));
*/


//  #4 Создайте маасив на 50 элементов и заполните каждый элемент его номером, не используя циклы 
// выведите каждый нечетный элемент в параграфе, а четный в диве.

/*
    const arr = new Array(50);
    function fillArray(arr) {
        
        for (let i = 0; i < arr.length; i++) {
        arr.splice(i, 1, i);
        };
        return arr;
    };

    document.getElementById("show").innerHTML = `Исходный массив: ${fillArray(arr)}`;

    let result = arr.map((item, index) => {
        if (index % 2 == 0) {
            return `<p style="background-color: pink; text-align: center">${item}</p>`
        } else {
            return `<div style="background-color: #d7d7d7; text-align: center">${item}</div>`
        }
    });

    document.write(result.join(""));
*/
/*------------------------------2-й вариант-----------------------------------------------------------*/

/*
    function fillArray(b) {
        let arr = [];
        for (let i = 0; i < b; i++) {
            arr.unshift(i);
            if (i % 2 == 0) {
                document.write("<div class='block'>" + i + "</div>");
            } else {
                document.write("<p class='par'>" + i + "</p>");
            }
        }
        return arr;
    }
    fillArray(50);
*/

// #5 Если переменная a больше нуля - то в ggg запишем функцию, которая выводит один !, 
// иначе запишем функцию, которая выводит два !

/*
    const func1 = () => {document.write("!" + "<br/>")};
    const func2 = () => {document.write("!!" + "<br/>")};

    function ggg(a) {
    let x =  a > 0 ? func1 : func2;
    return x();
    }
    ggg(25);

*/

// #6 Используя CallBack function создайте калькулятор который будет от пользователя принимать 2 числа 
// и знак арефметической операции. При вводе не числа или при делении на 0 выводить ошибку.

/*

    const add = (a, b) => a + b;
    const sub = (a, b) => a - b;
    const mult = (a, b) => a * b;
    const dvd = (a, b) => a / b;

    let num1 = parseInt(prompt("Введите первое число"));
    let num2 = parseInt(prompt("Введите второе число"));
    let oprt = prompt("Введите знак операции, которую хотите произвести", "+ - * /");

    function calculate(operator) {
        let result;
        if (isNaN(num1) || isNaN(num2)) {
            console.error(`Вы ввели ${num1}${oprt}${num2}. Введите числа.`);

        } else {
            if (operator == "+") {
                result = add(num1, num2)
            } else if (operator == "-") {
                result = sub(num1, num2);
            } else if (operator == "*") {
                result = mult(num1, num2);
            } else if (operator == "/") {
                if (num2 == 0) {
                    console.error("На 0 делить нельзя!");
                } else {
                    result = dvd(num1, num2);
                };
            };
        };

        return result;
    };

    document.write(`${num1}${oprt}${num2}=${calculate(oprt)}`);
*/
/*------------------------------2-й вариант-----------------------------------------------------------*/
/*

    function calc() {

        let numberOne = parseInt(prompt("Ведите первое число")) ;
        let operation = prompt("Введите знак операции, которую хотите произвести", "+ - * /");
        let numberTwo = parseInt(prompt("Введите второе число")) ;
        let result;

        function add(a, b) {
            return a + b;
        }

        function subtraction(a, b) {
            return a - b;
        }

        function multiplication(a, b) {
            return a * b;
        }

        function division(a, b) {
            return a / b;
        }
        if (isNaN(numberTwo) ||  isNaN(numberOne)) {
            alert("Данные не являются числом!");
        } else {
            if (operation == "+") {
                result = add(numberOne, numberTwo);
            } else if (operation == "-") {
                result = subtraction(numberOne, numberTwo);
            } else if (operation == "*") {
                result = multiplication(numberOne, numberTwo);

            } else {
                if (operation == "/" && numberTwo !== 0) {
                    result = division(numberOne, numberTwo);
                } else {
                    console.error("На ноль делить нельзя!");
                }
            }
        }

        document.write(` ${numberOne} ${operation} ${numberTwo} = ${result} `);

    }
    calc();
*/

// #7 Функция ggg принимает 2 параметра: анонимную функцию, которая возвращает 3 и анонимную функцию, 
// которая возвращает 4. Верните результатом функции ggg сумму 3 и 4.

/*

    const a = () => 3;
    const b = () => 4;

    const ggg = (func1, func2) => func1 + func2;

    document.write(ggg(a(), b()));

*/

// #8 Сделайте функцию, которая считает и выводит количество своих вызовов.


function closure () {
    let i = 1;    
    return () => i++;
   
};

let call = closure();

call(); // 1
call(); // 2
call(); // 3


