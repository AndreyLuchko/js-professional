// #1 Если переменная a равна 10, то выведите 'Верно', иначе выведите 'Неверно'.

/*

    const a = parseInt(prompt("Введите число"));

    if (a === 10) {
        document.write("Верно");
    } else {
        document.write("Неверно");
    };

*/

// #2 В переменной min лежит число от 0 до 59. Определите в какую четверть часа 
// попадает это число (в первую, вторую, третью или четвертую).

/*

    const min = prompt("Введите число от 0 до 59");

    if (min > 0 && min < 15) {
        document.write("Это первая четверть");
    } else if (min >= 15 && min < 30) {
        document.write("Это вторая четверть");
    } else if (min >= 30 && min < 45) {
        document.write("Это третья четверть");
    } else if (min >= 45 && min < 60) {
        document.write("Это четвертая четверть");
    } else {
        document.write("Введите число от 0 до 59");
    };

*/

// #3 Используя условные конструкции проверьте возвраст пользователя, если пользователю будет от 18 до 35 лет переведите его на сайт
// google.com через 2 секунды, если возвраст пользователя будет от 35 до 60 лет переведите его на сайт https://www.uz.gov.ua/, 
// если пользователею будет до 18 лет покажите ему первую серию лунтика из ютуба Выполните эту задачу с 
// используя if, switch, тернарный оператор

/*

    const age = prompt("Введите свой возраст.");

    switch (age) {
        case "0":
        case "1":
        case "2":
        case "3":
        case "4":
        case "5":
        case "6":
        case "7":
        case "8":
        case "9":
        case "10":
        case "11":
        case "12":
        case "13":
        case "14":
        case "15":
        case "16":
        case "17":
            // window.location.href = 'https://www.youtube.com/watch?v=3Hz3kHRRISU';  // открывается в существующем окне
            window.open("https://www.youtube.com/watch?v=3Hz3kHRRISU"); // открывается в новом окне
            break;
    };

    if (age >= 18 && age < 35) {
        document.write("<meta http-equiv='refresh' content='2;https://google.com'>");
    };

    (age >=35 && age < 60) ? document.write("<meta http-equiv='refresh' content='0;https://www.uz.gov.ua/'>") : null;

*/

// #4 Напишите код, который будет спрашивать логин (prompt). Если посетитель вводит «Админ»,
// то спрашивать пароль, если нажал отмена (escape) – выводить «Вход отменён».
// Пароль проверять так. Если введён пароль «Властелин», то выводить «Добро пожаловать!»,
// иначе – «Пароль неверен», при отмене – «Вход отменён».

/*

    let userLogin = prompt("Введите логин.");

    if (userLogin === "Админ") {
        let userPassword = prompt("Введите пароль.");
        switch (userPassword) {
            case "Властелин":
                alert("Добро пожаловать!");            
                break;
            case null:
                alert("Вход отменен.");
                break;
            default:
                alert("Пароль неверен.");
                break;
        };
    } else if (userLogin === null) {
        alert("Вход отменен.");
    } else {
        alert("Неверный логин");
    };

*/

