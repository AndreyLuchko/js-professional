// #1 Прямоугольник

/*

    for (let i = 0; i < 10; i++) {
        
        for (let j = 0; j < 20; j++) {
            document.write("$");  
        };
        document.write("<br/>");   
    };

*/

// #2 Прямоугольный треугольник

/*

    for (let i = 0; i < 10; i++) {
        
        for (let j = 0; j < 20; j++) {
            if (j <= i) {
                document.write("$");
            } else {
                document.write("&nbsp;&nbsp;");
            };
        };
        document.write("<br/>");
    };

*/

// #3 Равносторонний треугольник перевернутый

/*

    const a = 5;

    for (let i = 0; i < a ; i++) {
        

        for (let j = 0; j < (a * 2) ; j++) {
            if (j <= i || j >= (a * 2 - i)) {
                document.write("&nbsp;&nbsp;");
            } else {
                document.write("$");
            };
        };
        document.write("<br/>");
    };

*/


// #4 Равносторонний треугольник 

/*

    const mainCycle = 8;
    const secondCicle = mainCycle - 1;
    const innerCycle = mainCycle * 2 - 1;
    let score = mainCycle - 1;

    for (let i = 0; i < mainCycle ; i++) {
        for (let j = 0; j < innerCycle; j++) {
            if (j < score || j >= innerCycle - score) {
                document.write("&nbsp;&nbsp;");
            } else {
                document.write("$") ;
            };
        };
        score--;
        document.write("<br/>");
    }; 

*/

// #5 Равносторонний треугольник (из предидущего Вашего курса дз)

/*

    let xNum = 5;
    let yNum = 4;
    let str = 1;

    for (let i = 0; i < xNum; i++) {
        for (let j = 0; j < yNum; j++) {
            document.write("&nbsp;&nbsp;");
        }
        for (let h = 0; h < str; h++) {
            document.write("$");
        }
        yNum -= 1;
        str += 2;
        document.write("<br/>");
    };

*/

// #6 Ромб

/*
    const mainCycle = 10;
    const secondCicle = mainCycle - 1;
    const innerCycle = mainCycle * 2 - 1;
    let score = mainCycle - 1;


    for (let i = 0; i < mainCycle; i++) {
        for (let j = 0; j < innerCycle; j++) {
            if (j < score || j >= innerCycle - score) {
                document.write("&nbsp;&nbsp;");
            } else {
                document.write("$");
            };
        };
        score--;
        document.write("<br/>");
    };

    for (let i = 0; i < secondCicle; i++) {
        for (let j = 0; j < (secondCicle * 2); j++) {
            if (j <= i || j >= (secondCicle * 2 - i)) {
                document.write("&nbsp;&nbsp;");
            } else {
                document.write("$");
            };
        };
        document.write("<br/>");
    };

*/

// #7 Дано 2 числа А и В, где (А<В). Выведите на экран сумму всех чисел, расположенных в 
// числовом промежутке от А до В. 


/*
    const a = 2;
    const b = 10;
    let sum = 0;
    let k = a;

    for (let i = 0; i < b - a; i++) {  
        sum =  sum + k;
        k +=1;
    };
    document.write(`Сумма = ${sum}`);

*/

// #7.1 Выведите на экран все нечетные значения, расположенных в 
// числовом промежутке от А до В.

/*

    const a = 3;
    const b = 12;
    let k = a;

    for (let i = 0; i < b - a; i++) {
        k +=1;
        if (k % 2 == 0) continue;
    
        document.write(k + "<br/>");
    };

*/

// #8 Елочка

const numbersOfCycles = 3; // если добавить кол-во циклов, то надо дописать переменные и добавить вниз цикл.
const xNum = 10;  // размер елочки меняется здесь (четные цифры). Можно поменять на любые цифры, но тогда не такая красивая будет ))).
let xNum2 = xNum + 1;
let xNum3 = xNum2 + 1;

let str = 1;

let str2 = str + xNum;

let str3 = str2 + 2 * 2; 

let yNum = (xNum + xNum2 + xNum3 + numbersOfCycles - str)/2;
let yNum2 = (xNum + xNum2 + xNum3 + numbersOfCycles - str2)/2;
let yNum3 = (xNum + xNum2 + xNum3 + numbersOfCycles - str3)/2;

for (let i = 0; i < xNum; i++) {
    for (let j = 0; j < yNum; j++) {
        document.write("&nbsp;&nbsp;");
    }
    for (let h = 0; h < str; h++) {
        document.write("*");
    }
    yNum -= 1;
    str += 2;
    document.write("<br/>");
};

for (let i = 0; i < xNum2; i++) {
    for (let j = 0; j < yNum2; j++) {
        document.write("&nbsp;&nbsp;");
    }
    for (let h = 0; h < str2; h++) {
        document.write("*");
    }
    yNum2 -= 1;
    str2 += 2;
    document.write("<br/>");
};

for (let i = 0; i < xNum3; i++) {
    for (let j = 0; j < yNum3; j++) {
        document.write("&nbsp;&nbsp;");
    }
    for (let h = 0; h < str3; h++) {
        document.write("*");
    }
    yNum3 -= 1;
    str3 += 2;
    document.write("<br/>");
};
    