// * В папке calculator дана верстка макета калькулятора. Необходимо сделать этот калькулятор рабочим.
// * При клике на клавиши с цифрами - набор введенных цифр должен быть показан на табло калькулятора.
// * При клике на знаки операторов (`*`, `/`, `+`, `-`) на табло ничего не происходит - программа ждет введения 
//   второго числа для выполнения операции.
// * Если пользователь ввел одно число, выбрал оператор, и ввел второе число, то при нажатии как кнопки `=`, 
//   так и любого из операторов, в табло должен появиться результат выполенния предыдущего выражения.
// * При нажатии клавиш `M+` или `M-` в левой части табло необходимо показать маленькую букву `m` - это значит, 
//   то в памяти хранится число. Нажатие на `MRC` покажет число из памяти на экране. Повторное нажатие `MRC` должно 
//   очищать память.

const result = document.querySelector('.display input'),
    mark = document.querySelector('.display'),
    keys = document.querySelector('.keys');
let a = '', // первое число
    b = '', // второе число
    opperation = '', // математическая операция
    memory = ''; // память

// функция очистки всего
function clearAll() {
    result.value = '0';
    a = ''; // первое число
    b = ''; // второе число
    opperation = ''; // математическая операция  
}

// функция калькуляции
function calc(opp) {
    switch (opp) {
        case '+':
            a = +a + +b;
            break;
        case '-':
            a = a - b;
            break;
        case '*':
            a = a * b;
            break;
        case '/':
            if (b === '0') {
                a = 'Error' // ошибка при делении на 0
                result.value = a;
                return;
            }
            a = a / b;
            break;
    }
}

// добавление по клику значения кликнотого элемента в result
keys.addEventListener("click", (event) => {
    result.value = '';
    let e = event.target.value;

    //  исключение, если клик не на кнопку
    if (!event.target.classList.contains('button')) {

        if (a !== '' && opperation !== '' && b !== '') {
            result.value = b;
            return;
        } else if (a !== '' && opperation !== '') {
            result.value = a;
            return;
        } else {
            result.value = a;
            return;
        }
    }

    // добавление в память 
    if (e === 'm+' || e === 'm-') {
        result.value = a;
        memory = a;
        mark.classList.add('hide-before');
        console.log(memory);
        return;
    }
    if (e === 'mrc') {

        // удаление из памяти значения, если оно на экране калькулятора
        if (a === memory && memory !== '') {
            mark.classList.remove('hide-before');
            memory = '';
            result.value = a;
            return;
        }
        // операции калькуляции со значением памяти
        if (b === '' && a !== '') {
            b = memory;
            result.value = b;
            return;
        } else {
            a = memory
            result.value = a;
            return;
        }
    }

    // если нажимаем кнопку "С"
    if (e === 'C') {
        clearAll();
        return;
    }

    // если нажимаем кнопку с классом 'pink' (/, *, -, +)
    if (event.target.classList.contains('pink')) {

        if (a !== '' && b !== '' && opperation !== '') {
            calc(opperation);
            result.value = a;
            b = '';
            opperation = e;
            return
        } else {
            opperation = e;
            result.value = a;
            return;
        }
    }

    // заполнение первого (а) и второго (b) числа
    if (b === '' && opperation === '') {
        a += e;
        result.value = a;
    } else if (a !== '' && e !== '=') {
        b += e;
        result.value = b;
    }

    // если нажимаем кнопку '='
    if (e === "=") {
        calc(opperation);
        result.value = a;
        b = '';

    }
    console.log(a, b, opperation);

})