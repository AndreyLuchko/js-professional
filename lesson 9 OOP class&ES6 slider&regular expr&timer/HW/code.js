    // // Створення класу 

    // function Hamburger(size, stuffing) {

    //     if (size === undefined) {
    //         throw new HamburgerException(': no size given');
    //     } else if (size.param.split("_")[0] !== 'SIZE') {
    //         throw new HamburgerException(`: invalid size '${size.param}'`);
    //     } else {
    //         this.size = size;
    //         this.stuffing = stuffing;
    //         this.topping = [];
    //     }
    // }

    // // Створення констант

    // Hamburger.SIZE_SMALL = {
    //     param: 'SIZE_SMALL',
    //     price: 50,
    //     calories: 20
    // }
    // Hamburger.SIZE_LARGE = {
    //     param: 'SIZE_LARGE',
    //     price: 100,
    //     calories: 40
    // }
    // Hamburger.STUFFING_CHEESE = {
    //     param: 'STUFFING_CHEESE',
    //     price: 10,
    //     calories: 20
    // }
    // Hamburger.STUFFING_SALAD = {
    //     param: 'STUFFING_SALAD',
    //     price: 20,
    //     calories: 5
    // }
    // Hamburger.STUFFING_POTATO = {
    //     param: 'STUFFING_POTATO',
    //     price: 15,
    //     calories: 10
    // }
    // Hamburger.TOPPING_MAYO = {
    //     param: 'TOPPING_MAYO',
    //     price: 20,
    //     calories: 5
    // }
    // Hamburger.TOPPING_SPICE = {
    //     param: 'TOPPING_SPICE',
    //     price: 15,
    //     calories: 0
    // }


    // // Функція-конструктор для винятків HamburgerException

    // function HamburgerException(message) {
    //     this.message = message;
    //     this.name = "HamburgerException";
    // }

    // // Метод додавання добавок

    // Hamburger.prototype.addTopping = function (topping) {

    //     var isPresent = 0;
    //     for (let i = 0; i < this.topping.length; i++) {
    //         if (this.topping.includes(topping)) {
    //             isPresent++;
    //         }
    //     }
    //     if (!isPresent) {
    //         this.topping.push(topping);
    //     } else {
    //         throw new HamburgerException(`: duplicate topping '${topping.param}'`);
    //     }
    // }

    // // Метод прибирання добавок

    // Hamburger.prototype.removeTopping = function (topping) {

    //     for (let i = 0; i < this.topping.length; i++) {
    //         if (this.topping.includes(topping)) {
    //             this.topping.find((item, i) => (item.param === topping.param) ? this.topping.splice(i, 1) : false);
    //         } else {
    //             throw new HamburgerException(`: there are not such topping '${topping.param}' to delete it`);
    //         }
    //     }
    // }

    // // Метод отримання масиву добавок

    // Hamburger.prototype.getToppings = function () {
    //     return this.topping;
    // }

    // // Дізнатися розмір гамбургера

    // Hamburger.prototype.getSize = function () {
    //     return this.size.param;
    // }

    // // Дізнатися начинку гамбургера

    // Hamburger.prototype.getStuffing = function () {
    //     return this.stuffing.param;
    // }

    // //  Дізнатись ціну гамбургера

    // Hamburger.prototype.calculatePrice = function () {
    //     var totalPrice = this.size.price + this.stuffing.price + this.topping.reduce((sum, item) => sum + item.price, 0);
    //     return totalPrice;
    // }

    // // Дізнатися калорійність

    // Hamburger.prototype.calculateCalories = function () {
    //     var totalCalories = this.size.calories + this.stuffing.calories + this.topping.reduce((sum, item) => sum + item.calories, 0);
    //     return totalCalories;
    // }

    // /*****************************************************************************************************/



    // // Створення нового екземпляра класу Hamburger

    // var h = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);

    // // Виняток додавання добавки, за умови, що вони різні 

    // try {
    //     console.log(h);
    //     h.addTopping(Hamburger.TOPPING_MAYO);
    //     h.addTopping(Hamburger.TOPPING_SPICE);
    //     console.log(h);
    //     h.addTopping(Hamburger.TOPPING_MAYO);

    //     console.log(h);
    // } catch (err) {
    //     console.error(err.name, err.message);
    // }

    // // скільки коштує
    // console.log("Price: %f", h.calculatePrice(), "грн.");

    // // Запитаємо скільки там калорій

    // console.log("Calories: %f", h.calculateCalories());

    // // Перевірити, чи великий гамбургер?

    // console.log("Is hamburger large: %s", h.getSize() === 'SIZE_LARGE');

    // // Перевірка "скільки добавок"

    // console.log("Have %d toppings", h.getToppings().length);

    // // не передали обов'язкові параметри

    // try {
    //     var h1 = new Hamburger();
    // } catch (err) {
    //     console.error(err.name, err.message);
    // };

    // // передаємо некоректні значення, добавку замість розміру

    // try {
    //     var h2 = new Hamburger(Hamburger.TOPPING_MAYO, Hamburger.STUFFING_CHEESE);
    //     console.log(h2);
    // } catch (err) {
    //     console.error(err.name, err.message);
    // }


    // // Виняток прибрання добавки, за умови, що вона раніше була додана.

    // try {
    //     h.removeTopping(Hamburger.TOPPING_SPICE);
    //     console.log(h);

    //     h.removeTopping(Hamburger.TOPPING_SPICE);

    // } catch (err) {
    //     console.error(err.name, err.message);
    // }


// -------------------------------------------- ES6 ---------------------------------------------------------


// Клас для винятків HamburgerException

class HamburgerException {
    constructor(message) {
        this.message = message;
        this.name = "HamburgerException";
    }
}

// Клас для Hamburger

class Hamburger {

    // Створення констант

    static SIZE_SMALL = {
        param: 'SIZE_SMALL',
        price: 50,
        calories: 20
    }
    static SIZE_LARGE = {
        param: 'SIZE_LARGE',
        price: 100,
        calories: 40
    }
    static STUFFING_CHEESE = {
        param: 'STUFFING_CHEESE',
        price: 10,
        calories: 20
    }
    static STUFFING_SALAD = {
        param: 'STUFFING_SALAD',
        price: 20,
        calories: 5
    }
    static STUFFING_POTATO = {
        param: 'STUFFING_POTATO',
        price: 15,
        calories: 10
    }
    static TOPPING_MAYO = {
        param: 'TOPPING_MAYO',
        price: 20,
        calories: 5
    }
    static TOPPING_SPICE = {
        param: 'TOPPING_SPICE',
        price: 15,
        calories: 0
    }

    constructor(size, stuffing) {
        if (size === undefined) {
            throw new HamburgerException(': no size given');
        } else if (size.param.split("_")[0] !== 'SIZE') {
            throw new HamburgerException(`: invalid size '${size.param}'`);
        } else {
            this.size = size;
            this.stuffing = stuffing;
            this.topping = [];
        }
    }
    // Метод додавання добавок

    addTopping(topping) {

        var isPresent = 0;
        for (let i = 0; i < this.topping.length; i++) {
            if (this.topping.includes(topping)) {
                isPresent++;
            }
        }
        if (!isPresent) {
            this.topping.push(topping);
        } else {
            throw new HamburgerException(`: duplicate topping '${topping.param}'`);
        }
    }
    // Метод прибирання добавок

    removeTopping(topping) {

        for (let i = 0; i < this.topping.length; i++) {
            if (this.topping.includes(topping)) {
                this.topping.find((item, i) => (item.param === topping.param) ? this.topping.splice(i, 1) : false);
            } else {
                throw new HamburgerException(`: there are not such topping '${topping.param}' to delete it`);
            }
        }
    }
    // Метод отримання масиву добавок

    getToppings() {
        return this.topping;
    }
    // Дізнатися розмір гамбургера

    getSize() {
        return this.size.param;
    }
    // Дізнатися начинку гамбургера

    getStuffing() {
        return this.stuffing.param;
    }
    //  Дізнатись ціну гамбургера

    calculatePrice() {
        var totalPrice = this.size.price + this.stuffing.price + this.topping.reduce((sum, item) => sum + item.price, 0);
        return totalPrice;
    }
    // Дізнатися калорійність

    calculateCalories() {
        var totalCalories = this.size.calories + this.stuffing.calories + this.topping.reduce((sum, item) => sum + item.calories, 0);
        return totalCalories;
    }
}

/*****************************************************************************************************/

// Створення нового екземпляра класу Hamburger

    const h = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);

// Виняток додавання добавки, за умови, що вони різні 

    try {
        console.log(h);
        h.addTopping(Hamburger.TOPPING_MAYO);
        h.addTopping(Hamburger.TOPPING_SPICE);
        console.log(h);
        h.addTopping(Hamburger.TOPPING_MAYO);

        console.log(h);
    } catch (err) {
        console.error(err.name, err.message);
    }

// скільки коштує

    console.log("Price: %f", h.calculatePrice(), "грн.");

// Запитаємо скільки там калорій

    console.log("Calories: %f", h.calculateCalories());

// Перевірити, чи великий гамбургер?

    console.log("Is hamburger large: %s", h.getSize() === 'SIZE_LARGE');

// Перевірка "скільки добавок"

    console.log("Have %d toppings", h.getToppings().length);

// не передали обов'язкові параметри

    try {
        const h1 = new Hamburger();
        console.log(h1);
    } catch (err) {
        console.error(err.name, err.message);
    };

// передаємо некоректні значення, добавку замість розміру

    try {
        const h2 = new Hamburger(Hamburger.TOPPING_MAYO, Hamburger.STUFFING_CHEESE);
        console.log(h2);
    } catch (err) {
        console.error(err.name, err.message);
    }

// Виняток прибрання добавки, за умови, що вона раніше була додана.

    try {
        h.removeTopping(Hamburger.TOPPING_SPICE);
        console.log(h);

        h.removeTopping(Hamburger.TOPPING_SPICE);

    } catch (err) {
        console.error(err.name, err.message);
    }