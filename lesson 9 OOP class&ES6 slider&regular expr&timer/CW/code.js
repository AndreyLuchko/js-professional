// #1 Создайте приложение секундомер.
// * Секундомер будет иметь 3 кнопки "Старт, Стоп, Сброс"
// * При нажатии на кнопку стоп фон секундомера должен быть красным, старт - зеленый, 
// сброс - серый * Вывод счётчиков в формате ЧЧ:ММ:СС
// * Реализуйте Задание используя синтаксис ES6 и стрелочные функции



    let counterSec = 0,
        counterMin = 0,
        counterHour = 0,
        intevalHandler;

    const get = className => document.querySelector(className);
  
    const sec = get(".stopwatch-display span:last-child"),
        min = get(".stopwatch-display span:nth-child(2)"),
        hour = get(".stopwatch-display span:first-child"),
       display = get(".stopwatch-display");

    const count = () => {
    //    Seconds
        counterSec++;
        if (counterSec <= 9) {
            counterSec = '0' + counterSec;
            sec.innerText = counterSec;
        }
        if (counterSec > 9) {
            sec.innerText = counterSec;
        }

    // Minutes
        if (counterSec > 59) {
            counterMin++;
            min.innerText = '0' + counterMin;
            counterSec = 0;
            sec.innerText = '0' + counterSec;
        }
        if (counterMin > 9) {
            min.innerText = counterMin;
        }

    // Hours
        if (counterMin > 59) {
            counterHour++;
            hour.innerText = '0' + counterHour;
            counterMin = 0;
            min.innerText = '0' + counterMin;
        }
    // Total reset after 24H
        if (counterHour > 23) {
            sec.innerText = '00';
            min.innerText = '00';
            hour.innerText = '00';

            counterSec = 0;
            counterMin = 0;
            counterHour = 0;
        }
    }

    get(".stopwatch-control button:first-child").onclick = () => {

        intevalHandler = clearInterval(intevalHandler);  // умова, яка не дозволяє пришвидчити секундомір

        intevalHandler = setInterval(count, 1000);
        display.classList.add("green");
        display.classList.remove("red");
        display.classList.remove("silver");
    }
    get(".stopwatch-control button:nth-child(2)").onclick = () => {
        clearInterval(intevalHandler);
        display.classList.remove("green");
        display.classList.remove("silver");
        display.classList.add("red");
    }
    get(".stopwatch-control button:last-child").onclick = () => {
        sec.innerText = '00';
        min.innerText = '00';
        hour.innerText = '00';
        clearInterval(intevalHandler);
        counterSec = 0;
        counterMin = 0;
        counterHour = 0;
        display.classList.add("silver");
        display.classList.remove("red");
        display.classList.remove("green");
    }


// #2 Реализуйте программу проверки телефона
// * Используя JS Создайте поле для ввода телефона и кнопку сохранения
// * Пользователь должен ввести номер телефона в формате 000-000-00-00
// * Поле того как пользователь нажимает кнопку сохранить проверте правильность 
// ввода номера, если номер правильный сделайте зеленый фон и используя 
// document.location переведите пользователя на страницу 
// https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg 
// если будет ошибка отобразите её в диве до input.

/*
    const checkPhone = document.querySelector('.check-container'),
        inputField = document.createElement('input'),
        button = document.createElement('input'),
        err = document.createElement('p');

    inputField.setAttribute('type', "tel");
    inputField.setAttribute('placeholder', "000-000-00-00");
    button.setAttribute('type', "button");
    button.setAttribute('value', "Зберегти");

    err.textContent = '* Невірний формат введення'

    checkPhone.append(inputField);
    checkPhone.append(button);

    button.addEventListener('click', () => {
        if (/\d{3}-\d{3}-\d{2}-\d{2}\b/.test(inputField.value)) {
            inputField.style.background = 'rgb(13, 193, 61)';
            inputField.style.color = '#fff';
            document.location = 'https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg ';
        } else {
            inputField.before(err); 
            inputField.value = '';
        }
    });
*/

// #3 Слайдер
// Создайте слайдер который каждые 3 сек будет менять изображения
// Изображения для отображения
// https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg
// https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg
// https://naukatv.ru/upload/files/shutterstock_418733752.jpg
// https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg
// https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg

/*

    const arrSlides = document.querySelector('.slider-line'),
        slidesCount = document.querySelectorAll('.slider-line > div').length,
        mainSlider = document.querySelector('.slider'),
        width = parseInt(window.getComputedStyle(mainSlider).width);

    let activeSlideIndex = 0;

    function changeSlide() {

        activeSlideIndex++;
        if (activeSlideIndex == slidesCount) {
            arrSlides.style.left = 0;
            activeSlideIndex = 0;
        }
        arrSlides.style.transform = `translateX(-${(activeSlideIndex ) * width}px)`;

    }
    setInterval(changeSlide, 3000);
*/