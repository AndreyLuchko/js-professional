let url = "https://swapi.dev/api/starships";

// створення запиту методом fetch
const data = fetch(url);

// робота с запитом з Promise
let dataNext = data.then((res) => res.json());

// обробка Promise і отримання масива об'єктів
dataNext.then((resNext) => {
    let arrOfStarships = resNext.results;
    createCard(arrOfStarships); // запуск функції створення карток
})
.catch(err => console.error(err)); // відловлювання помилок

// функція створення карток
function createCard(arr) {
    arr.forEach(item => {
        let wrapper = document.createElement('div');

        wrapper.classList.add('wrapper');

        // додавання іконок на картки
        let icon;
        switch (item.name) {
            case "CR90 corvette":
                icon = "./img/corvette.png";
                break;
            case "Star Destroyer":
                icon = "./img/Star_Destroyer.png";
                break;
            case "Sentinel-class landing craft":
                icon = "./img/Sentinel_negvv.webp";
                break;
            case "Death Star":
                icon = "./img/Death_Star.png";
                break;
            case "Millennium Falcon":
                icon = "./img/Millennium_Falcon.png";
                break;
            case "Y-wing":
                icon = "./img/Y-wing.png";
                break;
            case "X-wing":
                icon = "./img/X-wing.png";
                break;
            case "TIE Advanced x1":
                icon = "./img/TIE_Advanced_x1.png";
                break;
            case "Executor":
                icon = "./img/Executor.png";
                break;
            case "Rebel transport":
                icon = "./img/Rebel_transport.png";
                break;
        }
        // створення карток
        wrapper.innerHTML = `
        <div class="card">
            <div class="card-img">
                <img src=${icon} alt="image">
            </div>
            <div class="card-name">
                <p>${item.model}</p>
            </div>
            <div class="card-descr">
                <div class="el first">
                    <div>${intToString(item.cost_in_credits)}</div>
                    <div>cost in credits</div>
                </div>
                <div class="el second">
                    <div>${item.length}</div>
                    <div>length</div>
                </div>
                <div class="el third">
                    <div>${item.max_atmosphering_speed}</div>
                    <div>max atm speed</div>
                </div>
            </div>
        </div>
        `;
        document.querySelector('.container').append(wrapper); // додаєм картку на сторінку
    })
}

// так як деяки числа дуже великі, то створюєм функцію перетворення великого числа до короткого вигляду
function intToString(value) {
    const suffixes = ["", "k", "m", "b", "t"];
    let suffixNum = Math.floor(("" + value).length / 3);
    let shortValue = parseFloat((suffixNum != 0 ? (value / Math.pow(1000, suffixNum)) : value).toPrecision(2));
    if (value === "unknown") {
        return value = 'N/A';
    }
    return shortValue + suffixes[suffixNum];
}