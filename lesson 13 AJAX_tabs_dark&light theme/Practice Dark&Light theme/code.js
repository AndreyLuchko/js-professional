let checkBox = document.querySelector('[type="checkbox"]');
console.log(checkBox);
let theme = document.querySelector('#theme');

document.addEventListener("DOMContentLoaded", () => {
    // theme.href = `${localStorage.getItem('href')}` // ось тут помилка була, при загрузці сторінки запитую стилі з LocalStorage, а іх там немає поки. Тепер працює.
    if(theme.getAttribute("href") === "./style/dark-theme.css") {
        checkBox.checked = true;
    }
})


checkBox.addEventListener('change', () => {
   
    if(theme.getAttribute("href") === "./style/dark-theme.css") {
      
        localStorage.removeItem("href");
        localStorage.setItem('href', './style/light-theme.css')  ;
        theme.href = `${localStorage.getItem('href')}`;
        
    } else {
        
        localStorage.removeItem("href");
        localStorage.setItem('href', './style/dark-theme.css');
        theme.href = `${localStorage.getItem('href')}`;
    }
})