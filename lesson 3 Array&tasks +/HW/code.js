// Создайте массив styles с элементами «Джаз» и «Блюз». 
// Добавьте «Рок-н-ролл» в конец.
// Замените значение в середине на «Классика». Ваш код для поиска значения 
// в середине должен работать для массивов с любой длиной.
// Удалите первый элемент массива и покажите его.
// Вставьте «Рэп» и «Регги» в начало массива.

/*

    const styles = ["Джаз", "Блюз"];

    document.write("Начальный массив: " + styles + "<br/>");

    let stylesPush = styles.push("Рок-н-Ролл");

    document.write("Добавили в конец 'Рок-н-Ролл': " + styles + "<br/>");

    let stylesSplice = styles.splice(Math.floor(styles.length/2), 1, "Классика");

    document.write("Заменили значение в середине на 'Классика': " + styles + "<br/>");

    let delFirstElem = styles.shift();

    document.write("Удалили первый элемент и показали его: " + delFirstElem + "<br/>");

    styles.unshift("Рэп", "Рэгги");

    document.write("Вставили 'Рэп' и 'Регги' в начало массива: " + styles);

*/

//  #1 Напишите функцию camelize(str), которая преобразует строки вида «my-short-string» 
// в «myShortString».
// То есть дефисы удаляются, а все слова после них получают заглавную букву.
// P.S. Подсказка: используйте split, чтобы разбить строку на массив символов,
//  потом переделайте всё как нужно и методом join соедините обратно.

/*

    function camelize(str) {
        

        let strSplit = str.split('-');


        let strMap = strSplit.map((item, index) => index == 0 ? item : item[0].toUpperCase() + item.slice(1));


        let result = strMap.join('');
        return result;
    };
    document.write(camelize("my-short-string"));

*/

// #2 Напишите функцию filterRange(arr, a, b), которая принимает массив arr, 
// ищет в нём элементы между a и b и отдаёт массив этих элементов.
// Функция должна возвращать новый массив и не изменять исходный.

/*

    function filterRange(arr, a, b) {
        let result = arr.filter(item => item >= a && item < b);
        return result;
    };

    document.write(filterRange([2, -8, 5, 10, 4, 8, 15], -10, 5));

*/

// #3 Напишите функцию filterRangeInPlace(arr, a, b), которая принимает массив arr 
// и удаляет из него все значения кроме тех, которые находятся между a и b. То есть, 
// проверка имеет вид a ≤ arr[i] ≤ b.
// Функция должна изменять принимаемый массив и ничего не возвращать.

/*

    function filterRangeInPlace(arr, a, b) {
        for (let i = 0; i < arr.length; i++) {
        
            if (a >= arr[i] || arr[i] >= b) {
                arr.splice(i, 1);
                i--;            
            };
            
        };  
    
    };
    let arr = [2, -8, 5, 10, 4, 8, 15];
    filterRangeInPlace(arr, 0, 5);
    document.write(arr);

*/

// #4 Сортировать в порядке по убыванию

/*

        let arr = [5, 2, 1, 15, -10, 8];

        // arr.sort().reverse();   //   не для всех массивов                        

        arr.sort((a, b) => b - a);

        document.write(arr);

*/


// #5 У нас есть массив строк arr. Нужно получить отсортированную копию, но оставить arr неизменённым.
// Создайте функцию copySorted(arr), которая будет возвращать такую копию.

/*

    let arr = ["HTML", "Javascript", "CSS"];

    function copySorted(arr) {
        let result = arr.slice().sort();
        return result;
    };

    let sorted = copySorted(arr);
    document.write(sorted);

*/

// #6 Создайте функцию конструктор Calculator, которая создаёт «расширяемые» объекты калькулятора.

// Задание состоит из двух частей.

// Во-первых, реализуйте метод calculate(str), который принимает строку типа "1 + 2" 
// в формате «ЧИСЛО оператор ЧИСЛО» (разделено пробелами) и возвращает результат.
//  Метод должен понимать плюс + и минус -.

// Затем добавьте метод addMethod(name, func), который добавляет в калькулятор новые операции.
// Он принимает оператор name и функцию с двумя аргументами func(a,b), которая описывает его.

// Например, давайте добавим умножение *, деление / и возведение в степень **:



    // Создал функцию- конструктор
    let Calculator = function () {
    //    добавил метод для сложения и вычитания, предварительно разбив строку на массив
        // this.calculate = function (str) {
        //     let arr = str.split(' ');
        //     let a = +arr[0];
        //     let b = +arr[2];
        //     let c = arr[1];
        //     if (c == "+") {
        //         return a + b;
        //     } else if (c == "-") {
        //         return a - b;
        //     };
        // };
        // не пойму как добавить метод для добавления операций в конструктор? 
        // Через объект (как в решении), в принципе понятноб, но хотел сам решить,
        // пусть и длинным способом, но на основе того, что знаю...

/*---------------------Продолжаю решать как надо )------------------------------------------ */ 

        // По условию, чтобы добавить метод в калькулятор, надо создать все таки массив с операторами 
        // и методами (функции), которые они будут выполнять. Т.о.:
        this.methods = {
            "+": (a, b) => a + b,
            "-": (a, b) => a - b
        } 
        //  А соответственно функцию calculate надо переписать
        this.calculate = function (str) {
            let arr = str.split(' ');
           let a = +arr[0];
           let b = +arr[2];
           let c = arr[1];  
            // возвращаем метод из массива с индексом "с", который является функцией.
            return this.methods[c](a, b); 
        }

        // Создаем метод(функцию), которая будет добавлять в массив methods другие методы ("*", "/", "**")
        this.addMethod = function (name, func) {
            this.methods[name] = func;
        };

    };


    let calk = new Calculator;
    calk.addMethod("*", (a, b) => a * b ); // в массиве есть "*"
    calk.addMethod("/", (a, b) => a / b ); // в массиве есть "/"
    calk.addMethod("**", (a, b) => a ** b ); // в массиве есть "**"


    document.write(calk.calculate("8 ** 2"));




// #7 У вас есть массив объектов user, и в каждом из них есть user.name. Напишите код, который преобразует их в массив имён.

// Например:

/*

    let vasya = { name: "Вася", age: 25 };
    let petya = { name: "Петя", age: 30 };
    let masha = { name: "Маша", age: 28 };

    let users = [ vasya, petya, masha ];

    let names = users.map(user => user.name );

    // alert( names ); // Вася, Петя, Маша

    document.write(names);

*/

// #8 У вас есть массив объектов user, и у каждого из объектов есть name, surname и id.
// Напишите код, который создаст ещё один массив объектов с параметрами id и fullName, 
// где fullName – состоит из name и surname.

// Например:

/*

    let vasya = { name: "Вася", surname: "Пупкин", id: 1 };
    let petya = { name: "Петя", surname: "Иванов", id: 2 };
    let masha = { name: "Маша", surname: "Петрова", id: 3 };

    let users = [ vasya, petya, masha ];

    let usersMapped = users.map(user => ({
        fullName: `${user.name} ${user.surname}`,
        id: user.id
    })); 
    document.write(usersMapped[2].id + "<br/>");
    document.write(usersMapped[2].fullName);

*/

/*
usersMapped = [
  { fullName: "Вася Пупкин", id: 1 },
  { fullName: "Петя Иванов", id: 2 },
  { fullName: "Маша Петрова", id: 3 }
]
*/

// alert( usersMapped[0].id ) // 1
// alert( usersMapped[0].fullName ) // Вася Пупкин

// Итак, на самом деле вам нужно трансформировать один массив объектов в другой. 
// Попробуйте использовать =>. Это небольшая уловка.

// #9 Напишите функцию sortByAge(users), которая принимает массив объектов со свойством age
//  и сортирует их по нему.

// Например:

/*

    let vasya = { name: "Вася", age: 25 };
    let petya = { name: "Петя", age: 30 };
    let masha = { name: "Маша", age: 28 };

    let arr = [ vasya, petya, masha ];

    let sortByAge = function(array) {
        
        array.sort((a, b) => a.age - b.age);
    };

    sortByAge(arr);

*/

// // теперь: [vasya, masha, petya]
// alert(arr[0].name); // Вася
// alert(arr[1].name); // Маша
// alert(arr[2].name); // Петя


// #10 Напишите функцию shuffle(array), которая перемешивает (переупорядочивает случайным образом) 
// элементы массива.
// Многократные прогоны через shuffle могут привести к разным последовательностям элементов.
//  Например:

// let arr = [1, 2, 3];

// shuffle(arr);
// // arr = [3, 2, 1]

// shuffle(arr);
// // arr = [2, 1, 3]

// shuffle(arr);
// // arr = [3, 1, 2]
// // ...
// Все последовательности элементов должны иметь одинаковую вероятность. 
// Например, [1,2,3] может быть переупорядочено как [1,2,3] или [1,3,2], или [3,1,2] и т.д.,
// с равной вероятностью каждого случая.

/*

    function shuffle(array) {
        array.sort(() => Math.random() - 0.5);
    }
    
    let arr = [1, 2, 3];
    shuffle(arr);
    alert(arr);
*/

// # 11 Напишите функцию getAverageAge(users), которая принимает массив объектов со свойством age
// и возвращает средний возраст.
// Формула вычисления среднего арифметического значения: (age1 + age2 + ... + ageN) / N.

// Например:

/*

    let vasya = { name: "Вася", age: 25 };
    let petya = { name: "Петя", age: 30 };
    let masha = { name: "Маша", age: 29 };

    let arr = [ vasya, petya, masha ];

    let getAverageAge = function (array) {
        let sum = array.reduce ((total, user) => total + user.age, 0);
        return sum / array.length;
    }

    alert( getAverageAge(arr) ); // (25 + 30 + 29) /

*/

// # 12 Пусть arr – массив строк.
// Напишите функцию unique(arr), которая возвращает массив, содержащий только уникальные 
// элементы arr.
// Например:

/*

    function unique(arr) {
    let result = arr.filter((item, index, array) => array.indexOf(item) === index);
    return  result;
    };

    let strings = ["кришна", "кришна", "харе", "харе",
    "харе", "харе", "кришна", "кришна", ":-O"
    ];

    document.write(unique(strings)); // кришна, харе, :-O

*/