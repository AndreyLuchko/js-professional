//  #1 При загрузке страницы - показать на ней кнопку с текстом "Нарисовать круг". Данная кнопка должна являться
//  единственным контентом в теле HTML документа, весь остальной контент должен быть создан и добавлен на страницу
//  с помощью Javascript/
//  При нажатии на кнопку "Нарисовать круг" показывать одно поле ввода - диаметр круга. При нажатии на кнопку 
//  "Нарисовать" создать на странице 100 кругов (10*10) случайного цвета. При клике на конкретный круг - этот круг 
//  должен исчезать, при этом пустое место заполняться, то есть все остальные круги сдвигаются влево.
// window.addEventListener('DOMContentLoaded', function () {

const btn = document.querySelector('#btn');
btn.addEventListener('click', () => {
    // создание главного div и присвоение ему класса

    let diametr = +prompt("Введите даметр круга");
    let blockWidth = diametr * 10;
    let mainBlock = document.createElement('div');
    mainBlock.classList.add('main');
    mainBlock.style.width = `${blockWidth}px`
    mainBlock.style.height = `${blockWidth}px`
    btn.after(mainBlock);

    for (let j = 0; j < 100; j++) {
        // выбор случайного цвета
        let r = Math.floor(Math.random() * 256);
        let g = Math.floor(Math.random() * 256);
        let b = Math.floor(Math.random() * 256);
        // создание div и присвоение класса для стилизации
        let round = document.createElement('div');
        mainBlock.appendChild(round);
        round.classList.add('inner');
        round.style.width = `${diametr}px`;
        round.style.height = `${diametr}px`;
        round.style.background = `rgb(${r},${g},${b})`;

    };

    // блок для удаления по клику
    let roundsList = document.querySelectorAll('.inner');

    roundsList.forEach((item, index) => {

        item.addEventListener('click', function () {
            roundsList[index].remove();
        });
    });

})