// #1 Создайте 2 инпута и одну кнопку. Сделайте так чтобы инпуты обменивались содержимым.

/*
    const input1 = document.querySelector('#input1');
        button = document.querySelector('#btn'),
        input2 = document.querySelector("#input2");


    button.onclick = () => {
        let variable = input1.value;
        input1.value = input2.value;
        input2.value = variable;
    }
*/

// #2 Создайте 5 дивов на странице затем используя getElementsByTagName и forEath 
// поменяйте дивам цвет фона.

/*
    const input2 = document.querySelector("#input2");
    const divBlock = document.createElement('div');
    input2.after(divBlock);

    for (let i = 0; i < 5; i++) {
        const element = document.createElement('div');
        divBlock.append(element);
        element.textContent = `div ${5 - i}`;
    }

    const [...divBg] = document.getElementsByTagName('div');

    divBg.forEach(item => item.style.background = "green");

*/

// #3 Создайте многострочное поле для ввода текста и кнопку. После нажатия кнопки
//  пользователем приложение должно сгенерировать тег div с текстом который был в 
// многострочном поле. многострочное поле следует очистить после переимещени информации

/*
    const textArea = document.querySelector('#textTest'),
        button = document.querySelector('#btn1');

        button.addEventListener('click', () => {
            const makeDiv = document.createElement('div');
            button.after(makeDiv);
            makeDiv.classList.add('result')
            makeDiv.textContent = textArea.value;
            textArea.value = ""; 
        });
*/

// #4 Создайте картинку и кнопку с названием "Изменить картинку" сделайте так чтобы при 
// загрузке страницы была картинка https://itproger.com/img/courses/1476977240.jpg 
// При нажатии на кнопку перый раз картинка заменилась на 
// https://itproger.com/img/courses/1476977488.jpg при втором нажатии чтобы картинка 
// заменилась на https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png


    const button = document.querySelector('#btn2'),
    arrImages = document.querySelectorAll(".picture img");

    let i = 0;

    button.addEventListener('click', () => {
        arrImages[i].classList = '';
        i++;
        if (i >= arrImages.length) {
            i = 0;
        }
        arrImages[i].classList = 'show';
    })


// #5 Создайте на странице 10 параграфов и сделайте так, чтобы при нажатии на параграф он исчезал.

/*
    const button = document.querySelector('#btn2'),
        mainDiv = document.createElement('div');
        mainDiv.classList = 'maindiv';

        button.after(mainDiv);

        for (let i = 0; i < 10; i++) {
            const element = document.createElement('p');
            mainDiv.append(element);
            element.textContent = `Параграф ${i + 1}`;
        }
        const arrOfP = document.querySelectorAll('.maindiv p');
        console.log(arrOfP);

        arrOfP.forEach((item, index) => {

            item.addEventListener('click', function () {
                arrOfP[index].remove();
            });
        });

*/
